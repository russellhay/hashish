from nose import tools as t

import hashish

class _TestObject(hashish.HashishMixin):
    __PASSWORD_FIELD__ = "password_hash"
    __SALT_COUNT__ = 4

    def __init__(self):
        self.password_hash = None

    @property
    def password_field(self):
        return self.__PASSWORD_FIELD__

    @property
    def salt_count(self):
        return self.__SALT_COUNT__

def test_functional():
    obj = _TestObject()

    t.eq_(obj.password_hash, None, "Password Hash not initialized")
    t.eq_(obj.password_field, "password_hash")
    t.eq_(obj.salt_count, 4)
    obj.password = "1234"
    t.assert_is_not_none(obj.password_hash,
        "Password Hash was not generated when password was set")
    t.assert_not_equal(obj.password_hash, "1234",
        "Password did not hash properly")
    t.eq_(obj.password_hash[:5], "{0};".format(obj.salt))

def test_salt_value():
    obj = _TestObject()

    t.eq_(len(obj.salt), obj.salt_count)

@t.raises(AttributeError)
def test_cannot_read_password():
    obj = _TestObject()
    print obj.password

@t.raises(ValueError)
def test_salt_too_long():
    obj = _TestObject()

    obj.salt="12345"

@t.raises(ValueError)
def test_salt_too_short():
    obj = _TestObject()
    obj.salt="123"

def test_salt_regeneration():
    obj = _TestObject()
    salt1 = obj.salt

    obj.salt = None
    salt2 = obj.salt

    t.assert_not_equal(salt1, salt2)

def test_setting_salt():
    obj = _TestObject()

    obj.salt = "1234"
    t.eq_(obj.salt, "1234")

def test_validate_valid():
    obj = _TestObject()

    obj.password = "1234"
    t.ok_(obj.validate("1234"))

def test_validate_invalid_no_exception():
    obj = _TestObject()

    obj.password = "1234"
    t.ok_(not obj.validate("12345"))

@t.raises(hashish.InvalidPasswordError)
def test_validate_invalid_exception():
    obj = _TestObject()

    obj.password = "1234"
    obj.validate("12345", throw=True)
